PROGRAM=hronpet1
CXX = g++
CXXFLAGS = -std=c++11 -Wall -pedantic -Wno-long-long -O0 -ggdb
SDL-LIB = -lSDL2 -lSDL2main -lSDL2_ttf -lSDL2_image

all : compile doc

run : compile
	./$(PROGRAM)

compile : $(PROGRAM)

doc : src/main.cpp src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/player.h src/consumable.h src/item.h src/weapon.h src/armor.h src/stats.h src/app.h
	doxygen doxyfile

$(PROGRAM) : objs/main.o objs/player.o objs/combat_entity.o objs/entity.o objs/map.o objs/stats.o objs/app.o objs/item.o objs/consumable.o objs/loot.o objs/weapon.o objs/armor.o objs/monster.o objs/map_position.o
	$(CXX) $(CXXFLAGS) $^ -o $@ $(SDL-LIB)

objs/main.o : src/main.cpp src/app.h src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/consumable.h src/item.h src/weapon.h src/armor.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/player.o : src/player.cpp src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/consumable.h src/item.h src/weapon.h src/armor.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/combat_entity.o : src/combat_entity.cpp src/combat_entity.h src/entity.h src/map_position.h src/stats.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@
	
objs/entity.o : src/entity.cpp src/entity.h src/map_position.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/map.o : src/map.cpp src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/player.h src/consumable.h src/item.h src/weapon.h src/armor.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/map_position.o : src/map_position.cpp src/map_position.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/stats.o : src/stats.cpp src/stats.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/app.o : src/app.cpp src/app.h src/item.h src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/armor.h src/consumable.h src/weapon.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/item.o : src/item.cpp src/item.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/weapon.o : src/weapon.cpp src/weapon.h src/item.h src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/armor.h src/consumable.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/armor.o : src/armor.cpp src/armor.h src/item.h src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/weapon.h src/consumable.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/consumable.o : src/consumable.cpp src/consumable.h src/item.h src/player.h src/map.h src/map_position.h src/monster.h src/combat_entity.h src/entity.h src/loot.h src/weapon.h src/armor.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/loot.o : src/loot.cpp src/loot.h src/entity.h src/map_position.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs/monster.o : src/monster.cpp src/monster.h src/combat_entity.h src/entity.h src/stats.h src/map_position.h | objs
	$(CXX) $(CXXFLAGS) -c $< -o $@

objs :
	mkdir objs
clean :
	-rm -rf $(PROGRAM) objs/ doc/ 2>/dev/null
