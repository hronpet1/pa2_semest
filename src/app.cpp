#include "app.h"
#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

CApp::CApp():
        currentMap(nullptr),
        currentPlayer(nullptr)
{
    Surf_Display = nullptr;
    Surf_Screen = nullptr;
    Running = true;
    Width_Display = WINDOW_WIDTH;
    Height_Display = WINDOW_HEIGHT;
}

int CApp::OnExecute()
{
    if(OnInit() == false)
    {
        return -1;
    }
    int action = menu();
    if(action == -1)
    {
        OnCleanup(); return 0;
    }
    while(Running)
    {
        SDL_Event Event;
        while(SDL_PollEvent(&Event))
        {
            switch(Event.type) {
                case SDL_QUIT: {
                    OnCleanup();
                    return 0;
                }
                case SDL_KEYDOWN: {
                    switch(Event.key.keysym.scancode)
                    {
                        case SDL_SCANCODE_W : {
                            if (currentPlayer->movePlayer(*currentMap, 'w')) {
                                if (Combat()) {
                                    ActionInfo("You won the fight.");
                                    break;
                                } else {
                                    ActionInfo("You died.Game over.");
                                    OnCleanup();
                                    return 0;
                                }
                            }
                            break;
                        }
                        case SDL_SCANCODE_A : {
                            if (currentPlayer->movePlayer(*currentMap, 'a')) {
                                if (Combat()) {
                                    ActionInfo("You won the fight.");
                                    break;
                                } else {
                                    ActionInfo("You died. Game over.");
                                    OnCleanup();
                                    return 0;
                                }
                            }
                            break;
                        }
                        case SDL_SCANCODE_S : {
                            if (currentPlayer->movePlayer(*currentMap, 's')) {
                                if (Combat()) {
                                    ActionInfo("You won the fight.");
                                    break;
                                } else {
                                    ActionInfo("You died. Game over.");
                                    OnCleanup();
                                    return 0;
                                }
                            }
                            break;
                        }
                        case SDL_SCANCODE_D : {
                            if (currentPlayer->movePlayer(*currentMap, 'd')) {
                                if (Combat()) {
                                    ActionInfo("You won the fight.");
                                    break;
                                } else {
                                    ActionInfo("You died.Game over.");
                                    OnCleanup();
                                    return 0;
                                }
                            }
                            break;
                        }
                        case SDL_SCANCODE_I :
                        {
                            if(printGUI()==1)
                            {
                                OnCleanup();
                                return 0;
                            }
                            break;
                        }
                        case SDL_SCANCODE_ESCAPE :
                                    {
                                        if(!PauseGame()) {
                                            int action = menu();
                                            if (action == -1) {
                                                OnCleanup();
                                                return 0;
                                            }
                                        }
                                        break;
                                    }
                        default : break;
                    }
                    break;
                }
            }
            if(currentPlayer->getMapName() != currentMap->getMapName())
                currentMap = currentPlayer->getMap(currentPlayer->getMapName());
        }
        currentMap->checkPosition(currentPlayer->getPos(), currentPlayer);
        currentMap->print(Surf_Screen);
        SDL_UpdateWindowSurface(Surf_Display);

    }
    OnCleanup();
    return 0;
}

bool CApp::OnInit()
{
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        std::cout << "SDL_Init failed: " <<  SDL_GetError() << std::endl;
        return false;
    }
    Surf_Display = SDL_CreateWindow("Shadows of Aster temple",
                                    SDL_WINDOWPOS_CENTERED,
                                    SDL_WINDOWPOS_CENTERED,
                                    Width_Display,
                                    Height_Display,
                                    SDL_WINDOW_SHOWN);
    if(TTF_Init() < 0)
    {
        std::cout << "TTF_Init failed" << std::endl;
        SDL_DestroyWindow(Surf_Display);
        SDL_Quit();
        return false;
    }
    if(IMG_Init(IMG_INIT_PNG) < 0)
    {
        std::cout << "IMG_Init failed" << std::endl;
        SDL_DestroyWindow(Surf_Display);
        TTF_Quit();
        SDL_Quit();
        return false;
    }
    if( Surf_Display == NULL)
    {
        std::cout << "Creating Window failed:" <<  SDL_GetError() << std::endl;
        SDL_DestroyWindow(Surf_Display);
        IMG_Quit();
        TTF_Quit();
        SDL_Quit();
        return false;
    }
    else
    {
        Surf_Screen = SDL_GetWindowSurface(Surf_Display);
    }
    return true;
}

void CApp::OnCleanup()
{
    delete currentPlayer;
    SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
    Running = false;
    SDL_DestroyWindow(Surf_Display);
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

void CApp::reprintGUI()
{
std::shared_ptr<SDL_Surface> inv (IMG_Load("surfaces/inventory.bmp"), SDL_FreeSurface);
SDL_Rect tmp;
tmp.x = 80;
tmp.y = 60;
SDL_BlitSurface (inv.get(), NULL, Surf_Screen, &tmp);
this->printStats();
this->printInventory();
SDL_UpdateWindowSurface(Surf_Display);
}

int CApp::printGUI() {
    reprintGUI();
    while (1) {
        SDL_Event Event;
        while (SDL_PollEvent(&Event)) {
            switch (Event.type) {
                case SDL_KEYDOWN :
                    switch (Event.key.keysym.scancode) {
                        case SDL_SCANCODE_B :
                        {                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                            return 0;
                        }

                        default:
                            break;
                    }
                case SDL_MOUSEBUTTONDOWN : {

                    int x = Event.button.x;
                    int y = Event.button.y;
                    /// if player presses button within inventory area
                    if (x > 326 && y > 93 && x < 520 && y < 225) {
                        x -= 327;
                        y -= 94;
                        unsigned int item = x / 32 + (y / 33) * 6;
                        if (currentPlayer->getInventory().size() > item) {
                            if (Event.button.button == SDL_BUTTON_RIGHT) {
                                if(!currentPlayer->getInventory().at(item).get()->UseItem(*currentPlayer)) ActionInfo("You can't use this item.");
                                reprintGUI();
                            }
                            if (Event.button.button == SDL_BUTTON_LEFT) {
                                currentMap->print(Surf_Screen);
                                reprintGUI();
                                currentPlayer->getInventory().at(item).get()->PrintItem(Surf_Screen, x + 332, y + 99);
                                SDL_UpdateWindowSurface(Surf_Display);
                            }
                        }
                    }
                    /// if player presses button within equiped area
                    else if (x > 219 && x < 253 && y > 236 && y < 370) {
                        unsigned int type = (y - 236) / 33;
                        if (currentPlayer->getEquiped(type) != nullptr) {
                            if (Event.button.button == SDL_BUTTON_RIGHT) {
                                currentPlayer->getEquiped(type)->UnequipItem(*currentPlayer);
                                reprintGUI();
                            }
                            if (Event.button.button == SDL_BUTTON_LEFT) {
                                reprintGUI();
                                currentPlayer->getEquiped(type)->PrintItem(Surf_Screen, x, y);
                                SDL_UpdateWindowSurface(Surf_Display);
                            }
                        }
                    }
                    else{
                        currentMap->print(Surf_Screen);
                        reprintGUI();
                    }
                    /// pressing skillpoint buttons
                    if (Event.button.button == SDL_BUTTON_LEFT) {
                        if (x > 489 && x < 507 && y > 287 && y < 305 && currentPlayer->skillpoint_avaible()) {
                            currentPlayer->change_skillpoint(-1);
                            currentPlayer->change_STR(1);
                            reprintGUI();
                        } else if (x > 489 && x < 507 && y > 311 && y < 329 && currentPlayer->skillpoint_avaible()) {
                            currentPlayer->change_skillpoint(-1);
                            currentPlayer->change_AGI(1);
                            reprintGUI();
                        } else if (x > 489 && x < 507 && y > 334 && y < 352 && currentPlayer->skillpoint_avaible()) {
                            currentPlayer->change_skillpoint(-1);
                            currentPlayer->change_INT(1);
                            reprintGUI();
                        }
                    }
                }
                    break;
            }
        }
    }
}


void CApp::printStats() const
{
    std::shared_ptr<TTF_Font> font (TTF_OpenFont("fonts/Herne-Bold.otf", 13),TTF_CloseFont);
    std::shared_ptr<TTF_Font> font_num (TTF_OpenFont("fonts/black_bloc.ttf", 14),TTF_CloseFont);
    SDL_Rect tmp;

    SDL_Color color = {0, 0, 0};
    tmp.x = 378; tmp.y = 253;
    std::shared_ptr<SDL_Surface> name (TTF_RenderText_Solid(font.get(), currentPlayer->getName().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (name.get(), NULL, Surf_Screen, &tmp);

    color = {125, 0, 0};
    tmp.x = 496; tmp.y = 245;
    std::shared_ptr<SDL_Surface> level (TTF_RenderText_Solid(font_num.get(), currentPlayer->getLevel().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (level.get(), NULL, Surf_Screen, &tmp);

    color = {200, 200, 0};
    tmp.x = 372; tmp.y = 265;
    std::shared_ptr<SDL_Surface> exp (TTF_RenderText_Solid(font_num.get(), currentPlayer->getExp().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (exp.get(), NULL, Surf_Screen, &tmp);

    color = {200,0,0};
    tmp.x = 367; tmp.y = 297;
    std::shared_ptr<SDL_Surface> health (TTF_RenderText_Solid(font_num.get(), currentPlayer->getHP().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (health.get(), NULL, Surf_Screen, &tmp);

    color = {0,0,200};
    tmp.x = 368; tmp.y = 321;
    std::shared_ptr<SDL_Surface> mana (TTF_RenderText_Solid(font_num.get(), currentPlayer->getMP().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (mana.get(), NULL, Surf_Screen, &tmp);

    color = {180,180,20};
    tmp.x = 382; tmp.y = 345;
    std::shared_ptr<SDL_Surface> gold (TTF_RenderText_Solid(font_num.get(), currentPlayer->getGold().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (gold.get(), NULL, Surf_Screen, &tmp);

    color = {140,0,0};
    tmp.x = 476; tmp.y = 284;
    std::shared_ptr<SDL_Surface> STR (TTF_RenderText_Solid(font_num.get(), currentPlayer->getSTR().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (STR.get(), NULL, Surf_Screen, &tmp);

    color = {0,150,0};
    tmp.x = 476; tmp.y = 308;
    std::shared_ptr<SDL_Surface> AGI (TTF_RenderText_Solid(font_num.get(), currentPlayer->getAGI().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (AGI.get(), NULL, Surf_Screen, &tmp);

    color = {0,0,150};
    tmp.x = 476; tmp.y = 331;
    std::shared_ptr<SDL_Surface> INT (TTF_RenderText_Solid(font_num.get(), currentPlayer->getINT().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (INT.get(), NULL, Surf_Screen, &tmp);

    color = {0,0,0};
    tmp.x = 486; tmp.y = 355;
    std::shared_ptr<SDL_Surface> armor (TTF_RenderText_Solid(font_num.get(), currentPlayer->getArmor().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (armor.get(), NULL, Surf_Screen, &tmp);

    if(currentPlayer->skillpoint_avaible())
    {
        std::shared_ptr<SDL_Surface> skillpoint (IMG_Load("surfaces/skillpoint.bmp"),SDL_FreeSurface);
        tmp.x = 490; tmp.y = 288;
        SDL_BlitSurface(skillpoint.get(),NULL, Surf_Screen, &tmp);
        tmp.x = 490; tmp.y = 312;
        SDL_BlitSurface(skillpoint.get(),NULL, Surf_Screen, &tmp);
        tmp.x = 490; tmp.y = 335;
        SDL_BlitSurface(skillpoint.get(),NULL, Surf_Screen, &tmp);
    }
}



void CApp::printInventory() const
{
    SDL_Rect tmp;
    tmp.x = 327; tmp.y = 93;
    int counter = 0;
    for(auto x : currentPlayer->getInventory())
    {
        SDL_BlitSurface ((std::shared_ptr<SDL_Surface>(IMG_Load(x.get()->getFileName().c_str()), SDL_FreeSurface)).get(), nullptr, Surf_Screen, &tmp);
        counter++;
        tmp.x = 327 + (counter % 6)*32;
        tmp.y = 93 + (counter / 6)*33;
        if(counter>23) break;
    }
    for(int i = 0; i<4 ; i++)
        if(currentPlayer->getEquipCopy(i) != nullptr)
        {
        tmp.x = 220; tmp.y = 237 + i*33;
            SDL_BlitSurface ((std::shared_ptr<SDL_Surface>(IMG_Load(currentPlayer->getEquipCopy(i)->getFileName().c_str()), SDL_FreeSurface)).get(), nullptr, Surf_Screen, &tmp);
        }
}

int CApp::menu()
{
    SDL_Rect tmp;
    tmp.x = 0; tmp.y = 0;
    std::shared_ptr<SDL_Surface> background (IMG_Load("surfaces/background.bmp"),SDL_FreeSurface);
    while(1)
    {
        SDL_BlitSurface(background.get(),NULL,Surf_Screen,&tmp);
        SDL_UpdateWindowSurface(Surf_Display);
        SDL_Event Event;
        while (SDL_PollEvent(&Event)) {
            switch(Event.type)
            {
                case SDL_QUIT :
                    return -1;
                case SDL_MOUSEBUTTONDOWN :
                {
                    if( Event.button.button == SDL_BUTTON_LEFT )
                    {
                        int x = Event.button.x;
                        int y = Event.button.y;
                        if(x > 180 && y > 235 && x < 380 && y < 275) // new game
                        {
                            if(newGame()==1) break;
                            while(1)
                            while(SDL_PollEvent(&Event)) {
                                switch (Event.type) {
                                    case SDL_KEYDOWN :
                                    {
                                        SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                                        return 0;}
                                    default :
                                        break;
                                }
                            }
                        }
                        if(x > 240 && y > 310 && x < 440 && y < 350) // load game
                        {
                            if(loadGame())
                            {SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                            return 0;}
                        }
                        if(x > 355 && y > 392 && x < 445 && y < 430) // quit
                        {
                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                            return -1;
                        }
                    }
                    break;
                }
            }
        }
    }
}


int CApp::newGame()
{
    SDL_Rect tmp;
    tmp.x = 0; tmp.y = 0;
    std::shared_ptr<SDL_Surface> tmpSurf (IMG_Load("surfaces/name.bmp"),SDL_FreeSurface);
    SDL_BlitSurface(tmpSurf.get(),NULL,Surf_Screen,&tmp);
    SDL_UpdateWindowSurface(Surf_Display);
    std::string new_name;
    std::shared_ptr<TTF_Font> font (TTF_OpenFont("fonts/Herne-Bold.otf", 35),TTF_CloseFont);
    while(1)
    {
        SDL_Event Event;
        while (SDL_PollEvent(&Event)) {
        switch(Event.type)
        {
            case SDL_QUIT: return -1;
            case SDL_MOUSEBUTTONDOWN:
            {
                if( Event.button.button == SDL_BUTTON_LEFT )
                {
                    int x = Event.button.x;
                    int y = Event.button.y;
                    if(x > 55 && y > 375 && x < 215 && y < 410) // confirm
                    {
                        if(new_name.length() == 0) break;
                        try
                        {
                            currentPlayer = new Player (new_name, 1, 1, 1, "firstMap", BaseStats(1),PlayerStats(1,1,5));
                        }
                        catch ( const char * error )
                        {
                            ActionInfo(error);
                            return 1;
                        }
                        if(!currentPlayer->changeMap("firstMap")) break;
                        currentMap = currentPlayer->getMap("firstMap");
                        SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                        std::shared_ptr<SDL_Surface> controls (IMG_Load("surfaces/controls.bmp"),SDL_FreeSurface);
                        SDL_Rect tmp;
                        tmp.x = 80; tmp.y = 60;
                        SDL_BlitSurface(controls.get(),NULL, Surf_Screen, &tmp);
                        SDL_UpdateWindowSurface(Surf_Display);
                        return 0;
                    }
                    if(x > 465 && y > 370 && x < 565 && y < 410) // back
                    {
                        SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                        return 1;
                    }
                }
                break;
            }
            case SDL_KEYDOWN :
            {
                if((Event.key.keysym.scancode > 3) & (Event.key.keysym.scancode < 30))
                {
                    char new_char = Event.key.keysym.scancode + 61;
                    if(new_name.length()<8)
                    new_name += new_char;
                    SDL_Color color = {255,150,150};
                    tmp.x = 250; tmp.y = 250;
                    std::shared_ptr<SDL_Surface> name (TTF_RenderText_Solid(font.get(), new_name.c_str(), color),SDL_FreeSurface);
                    SDL_BlitSurface (name.get(), NULL, Surf_Screen, &tmp);
                    SDL_UpdateWindowSurface(Surf_Display);
                }
                else if(Event.key.keysym.scancode == SDL_SCANCODE_BACKSPACE)
                {
                    if(new_name.length()>0)
                    new_name.pop_back();
                    SDL_Rect tmp;
                    tmp.x = 0; tmp.y = 0;
                    std::shared_ptr<SDL_Surface> tmpSurf (IMG_Load("surfaces/name.bmp"),SDL_FreeSurface);
                    SDL_BlitSurface(tmpSurf.get(),NULL,Surf_Screen,&tmp);
                    SDL_Color color = {255,150,150};
                    tmp.x = 250; tmp.y = 250;
                    std::shared_ptr<SDL_Surface> name (TTF_RenderText_Solid(font.get(), new_name.c_str(), color),SDL_FreeSurface);
                    SDL_BlitSurface (name.get(), NULL, Surf_Screen, &tmp);
                    SDL_UpdateWindowSurface(Surf_Display);
                }
            }
        }
        }
    }
}

bool CApp::PauseGame()
{
    std::shared_ptr<SDL_Surface> pauseMenu (IMG_Load("surfaces/pause.bmp"), SDL_FreeSurface);
    SDL_Rect tmp_rect;
    tmp_rect.x = 245; tmp_rect.y = 140;
    SDL_BlitSurface(pauseMenu.get(), NULL, Surf_Screen, &tmp_rect);
    SDL_UpdateWindowSurface(Surf_Display);
    while(1)
    {
        SDL_Event Event;
        while (SDL_PollEvent(&Event)) {
            switch (Event.type) {
                case SDL_MOUSEBUTTONDOWN:
                {
                    if(SDL_BUTTON_LEFT)
                    {
                        int x = Event.button.x;
                        int y = Event.button.y;
                        if( x > 260 && y > 154 && x < 380 && y < 195) // resume
                        {
                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                            return true;
                        }
                        if( x > 260 && y > 219 && x < 380 && y < 259) // save
                        {
                            SaveGame();
                        }
                        if( x > 260 && y > 287 && x < 380 && y < 327) // main menu
                        {
                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                            return false;
                        }
                    }
                }

            }
        }
    }

}

void CApp::SaveGame()
{
    currentPlayer->save();
}

bool CApp::loadGame()
{
    std::string load_name, load_map;
    int lvl, x, y, STR, AGI, INT, skill_point;
    std::ifstream load ("save/player_info.txt");
    if(!load.is_open()) {ActionInfo("Loading failed."); return false;}
    std::getline(load,load_name);
    std::getline(load,load_map);
    load >> x >> y;
    load >> lvl >> STR >> AGI >> INT >> skill_point;
    load.close();
    if(lvl == 0 || STR == 0 || AGI == 0 || INT == 0 ) {ActionInfo("Loading failed."); return false;}
    try
    {
        currentPlayer = new Player(load_name, lvl, x, y, load_map , BaseStats(STR),PlayerStats(AGI,INT,skill_point));
    }
    catch ( const char * error )
    {
        ActionInfo(error);
        return false;
    }
    if(!currentPlayer->load()) {ActionInfo("Loading failed."); return false;}
    currentMap = currentPlayer->getMap(currentPlayer->getMapName());
    return true;
}

bool CApp::Combat() {
    Monster *enemy = currentMap->getMonster(currentPlayer->getPos());

    SDL_Rect tmp_rect;
    std::ostringstream os;
    os << "surfaces/" << enemy->getFile() << "_big.bmp";
    std::shared_ptr<SDL_Surface> combat_bg(IMG_Load("surfaces/combat.bmp"), SDL_FreeSurface);
    std::shared_ptr<SDL_Surface> monster(IMG_Load(os.str().c_str()), SDL_FreeSurface);

    PrintCombatStats(enemy);
    SDL_UpdateWindowSurface(Surf_Display);
    while (1) {
        tmp_rect.x = 0;
        tmp_rect.y = 0;
        SDL_BlitSurface(combat_bg.get(), NULL, Surf_Screen, &tmp_rect);
        tmp_rect.x = 400; tmp_rect.y = 120;
        SDL_BlitSurface(monster.get(), NULL, Surf_Screen, &tmp_rect);
        PrintCombatStats(enemy);
        SDL_UpdateWindowSurface(Surf_Display);
        SDL_Event Event;
        while (SDL_PollEvent(&Event)) {
            switch (Event.type) {
                case SDL_QUIT :
                {
                    return false;
                }
                case SDL_MOUSEBUTTONDOWN :
                    if(Event.button.button == SDL_BUTTON_LEFT)
                    {
                        srand(time(nullptr));
                        int x = Event.button.x;
                        int y = Event.button.y;
                        if(x> 300 && x < 375 && y > 205 && y < 280)
                        {
                            /**
                             * Minimum player damage is equal to his weapon damage + 1
                             * Maximum player damage is equal to his weapon damage + agility stat + 1
                             */
                            int player_dmg = 0;
                            if(currentPlayer->getEquiped(0) != nullptr)
                            {int min = currentPlayer->getEquiped(0)->getDmg() + 1;
                            player_dmg = rand() % std::stoi(currentPlayer->getAGI()) + min;}
                            else
                                player_dmg = rand() % (std::stoi(currentPlayer->getAGI())+1) + 1;
                            if(!enemy->change_HP(-player_dmg)){
                                currentPlayer->change_exp(std::stoi(enemy->getExp()));
                                currentPlayer->change_gold(std::stoi(enemy->getGold()));
                                return true;
                            }
                            /// Enemy dmg is calculated by his dmg - player armor stat - can't go into negative numbers, minimal value is zero
                            int enemy_dmg = std::stoi(enemy->getDmg())-std::stoi(currentPlayer->getArmor());
                            if(enemy_dmg < 0) enemy_dmg = 0;
                            if(!currentPlayer->change_HP(-enemy_dmg)) return false;
                            PrintDamage(enemy,player_dmg,enemy_dmg);
                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                        }
                        if(x> 300 && x < 375 && y > 320 && y < 390)
                        {
                            if(!currentPlayer->change_MP(-5)) break;
                            int player_dmg = 0;
                            if(currentPlayer->getEquiped(0) != nullptr) {
                                int min = currentPlayer->getEquiped(0)->getDmg() + 1;
                                player_dmg = rand() % std::stoi(currentPlayer->getAGI()) + min;
                            }
                            else
                                player_dmg = rand() % (std::stoi(currentPlayer->getAGI())+1) + 1;
                            if(!enemy->change_HP(-player_dmg*2)) {
                                currentPlayer->change_exp(std::stoi(enemy->getExp()));
                                currentPlayer->change_gold(std::stoi(enemy->getGold()));
                                return true;
                            }

                            int enemy_dmg = std::stoi(enemy->getDmg())-std::stoi(currentPlayer->getArmor());
                            if(enemy_dmg < 0) enemy_dmg = 0;
                            if(!currentPlayer->change_HP(-enemy_dmg)) return false;
                            PrintDamage(enemy,player_dmg*2,enemy_dmg);
                            SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
                                                    }
                    }
            }
        }
    }
}

void CApp::PrintDamage(Monster * enemy, int player_dmg, int enemy_dmg)
{
    SDL_Color color = {0,0,0};
    SDL_Rect tmp_rect;
    std::shared_ptr<TTF_Font> font (TTF_OpenFont("fonts/Herne-Bold.otf", 14),TTF_CloseFont);
    std::ostringstream p_dmg;
    p_dmg << "You did " << player_dmg << " points of damage to " << enemy->getName() << ".";
    tmp_rect.x = 140; tmp_rect.y = 437;
    std::shared_ptr<SDL_Surface> text (TTF_RenderText_Solid(font.get(), p_dmg.str().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface(text.get(),NULL,Surf_Screen, &tmp_rect);

    std::ostringstream e_dmg;
    e_dmg << enemy->getName() << " did " << enemy_dmg << " damage to you.";
    std::shared_ptr<SDL_Surface> text_e (TTF_RenderText_Solid(font.get(), e_dmg.str().c_str(), color),SDL_FreeSurface);
    tmp_rect.x = 140; tmp_rect.y = 455;
    SDL_BlitSurface(text_e.get(),NULL,Surf_Screen, &tmp_rect);
    SDL_UpdateWindowSurface(Surf_Display);
    SDL_Delay(1300);
}

void CApp::PrintCombatStats(Monster * enemy)
{
    std::shared_ptr<TTF_Font> font (TTF_OpenFont("fonts/Herne-Bold.otf", 32),TTF_CloseFont);
    std::shared_ptr<TTF_Font> font_num (TTF_OpenFont("fonts/black_bloc.ttf", 25),TTF_CloseFont);
    SDL_Rect tmp_rect;
    tmp_rect.x = 60 ; tmp_rect.y = 50;
    SDL_Color color = {0, 200, 0};
    std::shared_ptr<SDL_Surface> name (TTF_RenderText_Solid(font.get(), currentPlayer->getName().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (name.get(), NULL, Surf_Screen, &tmp_rect);
    tmp_rect.x = 370 ; tmp_rect.y = 50;
    color = {200, 0, 0};
    std::shared_ptr<SDL_Surface> enemy_name (TTF_RenderText_Solid(font.get(), enemy->getName().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (enemy_name.get(), NULL, Surf_Screen, &tmp_rect);
    tmp_rect.x = 130 ; tmp_rect.y = 267;
    color = {0, 0, 0};

    std::shared_ptr<SDL_Surface> health (TTF_RenderText_Solid(font_num.get(), currentPlayer->getHP().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (health.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.y = 307;
    std::shared_ptr<SDL_Surface> mana (TTF_RenderText_Solid(font_num.get(), currentPlayer->getMP().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (mana.get(), NULL, Surf_Screen, &tmp_rect);

    std::ostringstream os;
    if(currentPlayer->getEquiped(0) != nullptr)
    os << currentPlayer->getEquiped(0)->getDmg() + 1 << " - " << currentPlayer->getEquiped(0)->getDmg() + 1 + std::stoi(currentPlayer->getAGI());
    else
        os << "1 - " << 1 + std::stoi(currentPlayer->getAGI());
    tmp_rect.y = 347; tmp_rect.x = 140;
    std::shared_ptr<SDL_Surface> dmg (TTF_RenderText_Solid(font_num.get(), os.str().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (dmg.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.y = 387; tmp_rect.x = 150;
    std::shared_ptr<SDL_Surface> armor (TTF_RenderText_Solid(font_num.get(), currentPlayer->getArmor().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (armor.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.x = 500; tmp_rect.y = 267;
    std::shared_ptr<SDL_Surface> health_e (TTF_RenderText_Solid(font_num.get(), enemy->getHP().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (health_e.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.x = 510; tmp_rect.y = 307;
    std::shared_ptr<SDL_Surface> dmg_e (TTF_RenderText_Solid(font_num.get(), enemy->getDmg().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (dmg_e.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.y = 347;
    std::shared_ptr<SDL_Surface> exp_e (TTF_RenderText_Solid(font_num.get(), enemy->getExp().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (exp_e.get(), NULL, Surf_Screen, &tmp_rect);

    tmp_rect.y = 387;
    std::shared_ptr<SDL_Surface> gold_e (TTF_RenderText_Solid(font_num.get(), enemy->getGold().c_str(), color),SDL_FreeSurface);
    SDL_BlitSurface (gold_e.get(), NULL, Surf_Screen, &tmp_rect);
}

void CApp::ActionInfo(std::string info)
{
    SDL_Color color = {0,0,0};
    std::shared_ptr<TTF_Font> font (TTF_OpenFont("fonts/Herne-Bold.otf", 20), TTF_CloseFont);
    std::shared_ptr<SDL_Surface> announce (IMG_Load("surfaces/announce.bmp"), SDL_FreeSurface);
    std::shared_ptr<SDL_Surface> text (TTF_RenderText_Solid(font.get(),info.c_str(),color), SDL_FreeSurface);
    SDL_Rect tmp_rect;
    tmp_rect.x = 95; tmp_rect.y = 215;
    SDL_BlitSurface(announce.get(), NULL, Surf_Screen, &tmp_rect);
    tmp_rect.x = 200 ; tmp_rect.y = 230;
    SDL_BlitSurface(text.get(), NULL, Surf_Screen, &tmp_rect);
    SDL_UpdateWindowSurface(Surf_Display);
    SDL_Delay(2500);
}
