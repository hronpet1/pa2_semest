#ifndef _CAPP_H_
#define _CAPP_H_
#include <SDL2/SDL.h>
#include "stdlib.h"
#include "player.h"
#include "monster.h"

/**
 *  Main game class that's responsible for running and displaying the game
 */

class CApp
{
private:
    bool Running;
    SDL_Window* Surf_Display;
    ///
    SDL_Surface* Surf_Screen;
    int Height_Display;
    int Width_Display;
    Map* currentMap;
    Player* currentPlayer;
public:
    CApp();
    /// Initialize SDL / TTF / IMG
    bool OnInit();
    /// Main game method, handles map movement, calls other methods
    int OnExecute();
    /// Main menu screen, able to load game, create new game or quit the game
    int menu();
    /// New game creation, name selection
    int newGame();
    bool loadGame();
    /// Pause menu screen - possibility to save game or go back to main menu
    bool PauseGame();
    /// Saves current game progress while deleting previous data
    void SaveGame();
    /// Called on collision with Monster,
    bool Combat();
    /// Prints messages about damage dealt between Player and Monster
    void PrintDamage(Monster * enemy, int player_dmg, int enemy_dmg);
    /// Prints neccessary stats of Player and Monster while in combat window
    void PrintCombatStats(Monster * enemy);
    void ActionInfo(std::string info);
    /// Prints GUI, handles player input while inventory is opened
    int printGUI();
    /// Reprints GUI when any change to any visible value happens
    void reprintGUI();
    void printStats() const;
    /// Prints player inventory and his equiped items
    void printInventory() const;
    /// Clean all initialized libraries, quit the game and close the window
    void OnCleanup();
};
#endif