#include "armor.h"
#include "player.h"

Armor::Armor(size_t id, int level, std::string name, std::string file, int armor, int type) :
Item(id,level,name,file), armor_value(armor), m_type(static_cast<Armor::armor_type >(type))
{}

bool Armor::UseItem(Player &itemUser)
{
if(m_level > std::stoi(itemUser.getLevel())) return false;
int type = static_cast<int>(m_type) + 1;
    Item* tmp = itemUser.getEquiped(type);
    itemUser.removeItem(this->m_id);
    if(tmp != nullptr) {
       tmp->UnequipItem(itemUser);
    }
    itemUser.getEquiped(type) = this;
    itemUser.change_armor(this->armor_value);
    return true;
}

void Armor::UnequipItem(Player &itemUser)
{
    itemUser.addToInventory(this->m_id);
    itemUser.change_armor(-(this->armor_value));
    itemUser.getEquiped(m_type + 1) = nullptr;
}

void Armor::PrintItem(SDL_Surface *surface, int x, int y)
{
    SDL_Rect position;
    position.x = x - 5 ; position.y = y - 5;
    SDL_Surface * tmp = IMG_Load("surfaces/item_info.bmp");
    SDL_BlitSurface(tmp, NULL, surface, &position);
    SDL_FreeSurface(tmp);
    position.x = x; position.y = y;
    std::ostringstream os;
    os << m_name ;
    printLine(surface, position, os);
    os << "Required level : " << m_level;
    position.y += 15;
    printLine(surface, position, os);
    os <<  "Bonus armor : " << armor_value;
    position.y += 15;
    printLine(surface, position, os);
}

