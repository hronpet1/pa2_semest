#ifndef SEMESTRALKA_NEW_ARMOR_H
#define SEMESTRALKA_NEW_ARMOR_H

#include "item.h"

/**
 *  Items with defensive values.
 */

class Armor : public Item{
public:
    /**
     *  Identifies spot where to equip armor
     */
    enum armor_type{
        HELMET,
        ARMOR,
        SHOES
    };
    /**
     * Constructor
     * @param id
     * @param level Required level
     * @param name Name of item
     * @param file Name of file for item
     * @param armor Defensive value
     * @param type Type of armor
     */
    Armor(size_t id, int level, std::string name, std::string file, int armor, int type);
    /**
     *
     * @param itemUser Reference to player
     * @return Returns false if requirements aren't met
     */
    bool UseItem(Player & itemUser);
    void UnequipItem(Player & itemUser);
    /**
     * Prints info about item
     * @param surface
     * @param x Coordinate for print
     * @param y Coordinate for print
     */
    void PrintItem(SDL_Surface* surface, int x, int y);
    int getArmor() const {return armor_value;}
private:
    int armor_value;
    armor_type m_type;

};
#endif //SEMESTRALKA_NEW_ARMOR_H
