#include "combat_entity.h"

CombatEntity::CombatEntity(std::string name, int level, int x, int y, BaseStats stats):
        Entity(name, x, y), m_level(level), stats_b(stats) {
}

bool CombatEntity::change_HP(int x)
{   if((stats_b.curHP == stats_b.maxHP) && x > 0) return false;
    if(stats_b.curHP + x < 0) {stats_b.curHP = 0; return false;}
    stats_b.curHP += x;
    if(stats_b.curHP>stats_b.maxHP)
        stats_b.curHP = stats_b.maxHP;
    return true;}