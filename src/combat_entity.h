#ifndef SEMESTRALKA_COMBATENTITY_H
#define SEMESTRALKA_COMBATENTITY_H
#include "entity.h"
#include "stats.h"

/**
 *  Derived from Entity , used for entities ( i.e. Player, Monster ) that need specific combat stats
 */

class CombatEntity : public Entity
{
public:
    CombatEntity(std::string name, int level, int x, int y, BaseStats stats);
    /** @ingroup Get
     *
     * @{
     */
    std::string getLevel() const {std::string s = std::to_string(m_level); return s;}
    std::string getHP() const { return stats_b.getHP();}
    /** @} */
    /** @ingroup Set
     *
     */
    bool change_HP(int x) ;

protected:
int m_level;
BaseStats stats_b;
};


#endif //SEMESTRALKA_COMBATENTITY_H
