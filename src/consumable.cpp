#include "consumable.h"
#include "player.h"
#include "combat_entity.h"

Consumable::Consumable(size_t id, int level, std::string name,std::string file, int eff, int amount):
Item(id, level, name, file), m_eff(static_cast<Consumable::effect>(eff)), m_amount(amount)
{}

bool Consumable::UseItem(Player &itemUser)
{
    if(m_level > std::stoi(itemUser.getLevel())) return false;
    switch (m_eff)
    {
        case HEALTH:
            if(!itemUser.change_HP(m_amount)) break;
            itemUser.removeItem(this->m_id);
            break;
        case MANA:
            if(!itemUser.change_MP(m_amount)) break;
            itemUser.removeItem(this->m_id);
            break;
        case STRENGTH:
            itemUser.change_STR(m_amount);
            itemUser.removeItem(this->m_id);
            break;
        case AGILITY:
            itemUser.change_AGI(m_amount);
            itemUser.removeItem(this->m_id);
            break;
        case INTELLIGENCE:
            itemUser.change_INT(m_amount);
            itemUser.removeItem(this->m_id);
            break;
    }
    return true;
}

void Consumable::PrintItem(SDL_Surface *surface, int x, int y)
{
    SDL_Rect position;
    position.x = x - 5 ; position.y = y - 5;
    SDL_Surface * tmp = IMG_Load("surfaces/item_info.bmp");
    SDL_BlitSurface(tmp, NULL, surface, &position);
    SDL_FreeSurface(tmp);
    position.x = x; position.y = y;
    std::ostringstream os;
    os << m_name ;
    printLine(surface, position, os);
    os << "Required level : " << m_level;
    position.y += 15;
    printLine(surface, position, os);
    if(m_eff == MANA || m_eff == HEALTH)
    {
        os << "Restores " << m_amount << " " << getTextForEnum(static_cast<int>(m_eff));
        position.y += 15;
        printLine(surface, position, os);
    }
    else {
        os << "Increases " << getTextForEnum(static_cast<int>(m_eff)) ;
        position.y += 15;
        printLine(surface, position, os);
    }
}