#ifndef SEMESTRALKA_NEW_CONSUMABLE_H
#define SEMESTRALKA_NEW_CONSUMABLE_H

#include "item.h"

class Player;
/**
 *  Items that disappear after use
 */
class Consumable : public Item
{
public:
    enum effect{
        HEALTH,
        MANA,
        STRENGTH,
        AGILITY,
        INTELLIGENCE
    };
    std::string EnumStrings[5] = { "Health", "Mana", "Strength", "Agility", "Intelligence" };

    std::string getTextForEnum( int enumVal )
    {
        return EnumStrings[enumVal];
    }
    /**
     * Constructor
     * @param eff  Type of attribute that it gives
     * @param amount How many points of given attribute it gives
     */
Consumable (size_t id, int level, std::string name,std::string file, int eff, int amount );
bool UseItem(Player & itemUser);
void UnequipItem(Player & itemUser) {};
void PrintItem(SDL_Surface* surface, int x, int y);

private:
    effect m_eff;
    int m_amount;
};

#endif //SEMESTRALKA_NEW_CONSUMABLE_H
