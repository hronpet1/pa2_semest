#ifndef SEMESTRALKA_ENTITY_H
#define SEMESTRALKA_ENTITY_H
#include "map_position.h"
#include <memory>

/**
 *  Base class for all entities appearing in map
 */

class Entity
{
public:
    Entity(std::string name,int x, int y);
    /** @ingroup Get
     *
     */
    std::string getName() const {return m_name;}
    /**
     *  Returns current position of entity in Map
     */
    MapPosition getPos() const {return position;}
protected:
    std::string m_name;
    MapPosition position;
};

#endif //SEMESTRALKA_ENTITY_H

