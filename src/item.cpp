#include "item.h"

Item::Item(size_t id, int level, std::string name, std::string file):
m_id(id), m_level(level), m_name(name), file_name(file)
{}


void Item::printLine(SDL_Surface *surface, SDL_Rect pos, std::ostringstream & os)
{

    TTF_Font* font = TTF_OpenFont("fonts/Herne-Bold.otf", 13);
    SDL_Color color = {0,0,0};
    SDL_Surface* armor = TTF_RenderText_Solid(font , os.str().c_str(), color);
    SDL_BlitSurface(armor, NULL, surface, &pos);
    SDL_FreeSurface(armor);
    TTF_CloseFont(font);
    os.str("");
    os.clear();
}