
#ifndef SEMESTRALKA_ITEM_H
#define SEMESTRALKA_ITEM_H
#include <string>
#include <sstream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

class Player;

/**
 *  Base class for all items.
 */

class Item
{
public:
    /**
    //     * Constructor
    //     * @param id
    //     * @param level Required level
    //     * @param name Name of item
    //     * @param file Name of file for item
     */
    Item(size_t id, int level, std::string name, std::string file);
    /// virtual use method, called when you right click item in your inventory
    virtual bool UseItem(Player & itemUser) = 0;
    /// virtual method for equipable items (i.e. Weapon and Armor)
    virtual void UnequipItem(Player & itemUser) = 0;
    /// virtual print method, called on left click on item
    virtual void PrintItem(SDL_Surface* surface, int x, int y) = 0;
    /// text printed line by line, since TTF_Render doesn't support newlines
    void printLine(SDL_Surface *surface, SDL_Rect pos, std::ostringstream & os);
    virtual int getDmg() const {return 0;};
    /** @ingroup Get
     *
     * @{
     */
    std::string getName() const {return m_name;}
    std::string getFileName() const {return "item_image/" + file_name;}
     /** @} */
    size_t getId() const {return m_id;}

protected:
    size_t m_id;
    int m_level;
    std::string m_name;
    std::string file_name;
};


#endif //SEMESTRALKA_ITEM_H
