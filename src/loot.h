
#ifndef SEMESTRALKA_NEW_LOOT_H
#define SEMESTRALKA_NEW_LOOT_H
#include "entity.h"

/**
 *  Derived from Entity, gives Items to Player, each Lootbox has exactly one item
 */
class LootBox : public Entity
{
public:
    LootBox(int x, int y, size_t item);
    size_t getItem() const {return item_id;}
private:
    size_t item_id;
};


#endif //SEMESTRALKA_NEW_LOOT_H
