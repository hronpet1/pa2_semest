#include <iostream>
#include <fstream>
#include "map.h"
#include "player.h"

Map::Map(std::string mapName) {
    name_map = mapName;
    std::ifstream mapToLoad("maps/" + mapName + "/map.txt");
    if (!mapToLoad.is_open()) throw "missing map file";
    mapToLoad >> m_height >> m_width;
    mapToPrint.resize(m_height);
    int field;
    for (unsigned int i = 0; i < m_height; ++i) {
        mapToPrint[i].resize(m_width);
        for (unsigned int j = 0; j < m_width; ++j) {
            mapToLoad >> field;
            mapToPrint.at(i).at(j) = static_cast<occupiedBy>(field);
        }
    }
    mapToLoad.close();
    std::ifstream portalsToLoad("maps/" + mapName + "/portals.txt");
    if (!portalsToLoad.is_open()) throw "missing portals file";
    int x, y;
    std::string new_map_name;
    while (!portalsToLoad.eof()) {
        portalsToLoad >> x >> y;
        portalsToLoad >> new_map_name;
        portals.insert(std::make_pair(MapPosition(x, y), new_map_name));
    }
    portalsToLoad.close();

std::shared_ptr<SDL_Surface> tmp_ground(IMG_Load("surfaces/black.bmp"), SDL_FreeSurface);
    surfaces.insert(std::make_pair(0,tmp_ground));
std::shared_ptr<SDL_Surface> tmp(IMG_Load("surfaces/dirt.bmp"), SDL_FreeSurface);
    surfaces.insert(std::make_pair(1,tmp));
std::shared_ptr<SDL_Surface> tmp1(IMG_Load("surfaces/chest.bmp"), SDL_FreeSurface);
    surfaces.insert(std::make_pair(2,tmp1));
std::shared_ptr<SDL_Surface> tmp2(IMG_Load("surfaces/portal.bmp"), SDL_FreeSurface);
    surfaces.insert(std::make_pair(6,tmp2));
std::shared_ptr<SDL_Surface> tmp3(IMG_Load("surfaces/player.bmp"), SDL_FreeSurface);
    surfaces.insert(std::make_pair(5,tmp3));

    std::ifstream lootbox("maps/" + mapName + "/lootboxes.txt");
    if(!lootbox.is_open()) throw "missing lootbox file";
    size_t new_id;
    int n_x,n_y;
    while(!lootbox.eof())
    {
        lootbox >> n_x >> n_y >> new_id ;
        std::shared_ptr<LootBox> tmp = std::make_shared <LootBox>(n_x,n_y,new_id);
        map_lootboxes.insert(std::make_pair(MapPosition(n_x,n_y),tmp));
    }
    lootbox.close();

    std::ifstream monster ("maps/" + mapName + "/monsters.txt");
    if(!monster.is_open()) throw "missing monsters file";
    std::string monster_name, monster_file, newline;
    int level, strength, exp, gold, damage;
    while(!monster.eof())
    {
        std::getline(monster, monster_name);
        std::getline(monster, monster_file);
        monster >> level >> x >> y >> strength >> damage >> exp >> gold;
        auto tmp_monster = std::make_shared<Monster>(monster_name,monster_file,level,x,y,BaseStats(strength), damage, exp, gold);
        std::getline(monster, newline);
        std::ostringstream os;
        os << "surfaces/" << monster_file << ".bmp";
        std::shared_ptr<SDL_Surface> monster_surf(IMG_Load(os.str().c_str()), SDL_FreeSurface);
        map_monsters.push_back(std::make_pair(monster_surf,tmp_monster));
    }

}


int Map::checkPosition(const MapPosition & player_pos, Player * player) {
    if(player_pos.m_x < 0 || player_pos.m_x >= m_height || player_pos.m_y < 0 || player_pos.m_y >= m_width)
        return 1;
    switch (mapToPrint.at(player_pos.m_x).at(player_pos.m_y)) {
        case EMPTY : {
            mapToPrint.at(player_pos.m_x).at(player_pos.m_y) = static_cast<occupiedBy>(5);
            return 0;
        }
        case TERRAIN :
            return 1;
        case LOOTBOX : {
            if(map_lootboxes.find(player_pos) == map_lootboxes.end()) return 1;
            mapToPrint.at(player_pos.m_x).at(player_pos.m_y) = static_cast<occupiedBy>(0);
            return 2;
        }
        case NPC : // todo
            return 1;
        case MONSTER :
        {
            mapToPrint.at(player_pos.m_x).at(player_pos.m_y) = static_cast<occupiedBy>(5);
            return 4;}
        case MAPCHANGE :
            for (auto x : portals) {
                if (x.first == player_pos) {
                    player->changeMap(x.second);
                    return 3;
            }
    }

        default:
            return 1;
    }
}

void Map::print(SDL_Surface* screen)
{
    for (unsigned int i = 0; i < m_height; i++) {
        for (unsigned int j = 0; j < m_width; j++) {
            switch (mapToPrint.at(i).at(j)) {
                case EMPTY:
                {this->setSurface(i, j, screen, 0);
                    break;}
                case TERRAIN:
                {this->setSurface(i, j, screen, 1);
                    break;}
                case LOOTBOX:
                {this->setSurface(i, j, screen, 2);
                    break;}
                case NPC:
                {this->setSurface(i, j, screen, 0);
                    break;}
                case MONSTER:
                {this->setSurfaceMonster(i, j, screen);
                    break;}
                case PLAYER:
                {this->setSurface(i, j, screen, 5);
                    break;}
                case MAPCHANGE:
                {this->setSurface(i, j, screen, 6);
                    break;}
            };
        }
    }
}

void Map::setSurface(int y, int x, SDL_Surface* dst ,int type)
{
    SDL_Rect tmp;
    tmp.x = x * 32;
    tmp.y = y * 32;
    SDL_BlitSurface((surfaces.find(type)->second).get(), NULL, dst, &tmp);
}

void Map::setSurfaceMonster(int y, int x, SDL_Surface *dst)
{
    SDL_Rect tmp;
    tmp.x = x * 32;
    tmp.y = y * 32;
    for( auto iter : map_monsters)
       if( iter.second.get()->getPos() == MapPosition(y,x)) {
           SDL_BlitSurface(iter.first.get(), NULL, dst, &tmp);
       }
}

size_t Map::findItem(MapPosition x) const
{
    return map_lootboxes.find(x)->second.get()->getItem();
}

bool Map::reloadMap(std::string path)
{
    std::ifstream tmp(path);
    int x;
    if(!tmp.is_open()) return false;
    for (unsigned int i = 0; i < m_height; ++i) {
        mapToPrint[i].resize(m_width);
        for (unsigned int j = 0; j < m_width; ++j) {
            tmp >> x;
            mapToPrint.at(i).at(j) = static_cast<occupiedBy>(x);
        }
    }
    tmp.close();
    return true;
}

Monster* Map::getMonster(MapPosition position) const
{
    for(auto x : map_monsters)
        if(x.second.get()->getPos() == position)
            return x.second.get();
    return nullptr;
}