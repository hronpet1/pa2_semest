#ifndef SEMESTRALKA_MAP_H
#define SEMESTRALKA_MAP_H

#include "map_position.h"
#include "monster.h"
#include "loot.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <vector>
#include <memory>
#include <map>

class Player;
/**
 *  Map representation, includes all entities in it
 */
class Map
{
public:
    enum occupiedBy{
        EMPTY = 0,
        TERRAIN = 1,
        LOOTBOX = 2,
        NPC = 3,
        MONSTER = 4,
        PLAYER = 5,
        MAPCHANGE = 6
    };
    Map(std::string mapName);
    /**
     *     @return Type of entity occupying current position
     */
    int checkPosition(const MapPosition & player_pos, Player * player);
    void updateLastPosition(int x, int y) { mapToPrint.at(x).at(y) = static_cast<occupiedBy>(0);}
    void print(SDL_Surface* screen);
    /// Blits surface image on certain position, chosen from surfaces
    void setSurface(int y, int x, SDL_Surface* dst, int type);
    /// Source surface found by connecting positions of Monster and position based on parameters
    void setSurfaceMonster(int y, int x, SDL_Surface* dst);
    std::string getMapName() const { return name_map;}
    unsigned int getHeight() const { return m_height;}
    unsigned int getWidth() const { return m_width;}
    /// Returns type of entity that's on certain position in the Map
    int getMapPoint(int x, int y) { return static_cast<int>(mapToPrint.at(x).at(y));}
    /// Returns id of item, which is in the lootbox on certain position
    size_t findItem(MapPosition x) const;
    /// Loads new 2D vector from file
    bool reloadMap(std::string path);
    /// Returns pointer to Monster on position given by parameter
    Monster* getMonster(MapPosition position) const;
private:
    std::string name_map;
    unsigned int m_width;
    unsigned int m_height;
    /// 2D vector representation of map
    std::vector<std::vector<occupiedBy>> mapToPrint;
    /// position + name of new map
    std::map<MapPosition,std::string> portals;
    /// position + LootBox object
    std::map<MapPosition,std::shared_ptr<LootBox>> map_lootboxes;
    /// vector of Monster + their image pairs
    std::vector<std::pair<std::shared_ptr<SDL_Surface>,std::shared_ptr<Monster>>> map_monsters;
    /// map of loaded surfaces that will be used frequently
    std::map<int,std::shared_ptr<SDL_Surface>> surfaces;
};




#endif //SEMESTRALKA_MAP_H
