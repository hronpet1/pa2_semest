#include "map_position.h"


MapPosition::MapPosition(int x, int y): m_x(x),m_y(y){}


bool MapPosition::operator==(MapPosition x) const {
    if(m_x == x.m_x && m_y == x.m_y)
        return true;
    return false;
}


bool MapPosition::operator<(MapPosition x) const
{
    if(m_x>x.m_x) return true;
    if(m_x<x.m_x) return false;
    else
    {
        if(m_y>x.m_y) return true;
        else return false;
    }
}