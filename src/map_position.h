
#ifndef SEMESTRALKA_MAPPOSITION_H
#define SEMESTRALKA_MAPPOSITION_H

/**
 * Class defining position in map by coordinates
 */
class MapPosition
{
public:
    MapPosition(int x, int y);
    bool operator== (MapPosition x) const;
    bool operator< (MapPosition x) const;
    unsigned int m_x;
    unsigned int m_y;
};


#endif //SEMESTRALKA_MAPPOSITION_H
