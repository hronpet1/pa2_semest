#include "monster.h"

Monster::Monster(std::string name,std::string file, int level, int x, int y, BaseStats stats, int dmg, int exp_reward, int gold_reward)
: CombatEntity(name,level,x,y,stats),
  m_exp_reward(exp_reward),
  m_gold_reward(gold_reward),
  damage(dmg),
  file_name(file)
{}
