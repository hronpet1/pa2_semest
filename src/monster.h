#ifndef SEMESTRALKA_NEW_MONSTER_H
#define SEMESTRALKA_NEW_MONSTER_H
#include "combat_entity.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"


/**
 * Hostile monsters, derived from CombatEntity
 */
class Monster : public CombatEntity
{
public:
    Monster(std::string name, std::string file, int level, int x, int y, BaseStats stats, int dmg, int exp_reward, int gold_reward);
    /**@ingroup Get
     *
     * @{
     */
    std::string getFile() const {return file_name;}
    std::string getDmg() const {std::ostringstream os ; os << damage ; return os.str();}
    std::string getExp() const {std::ostringstream os ; os <<  m_exp_reward; return os.str();}
    std::string getGold() const {std::ostringstream os ; os << m_gold_reward; return os.str();}
    /** @} */
    private:
    int m_exp_reward;
    int m_gold_reward;
    int damage;
    std::string file_name;
};

#endif //SEMESTRALKA_NEW_MONSTER_H
