#include "player.h"

Player::Player(std::string name, int level, int x, int y, std::string mapname, BaseStats stats_b,
               PlayerStats stats_p):
CombatEntity(name, level, x, y , stats_b), current_map(mapname), player_stat(stats_p)
{
    player_stat.exp_needed = level * 80;
    std::ifstream items("items.txt");
    if (!items.is_open()) throw "failed to open items.txt";
    std::string line;
    size_t new_id = 0;
    int new_level = 0;
    std::string new_name;
    std::string new_file_name;
    int effect = 0;
    int amount = 0;
    int stat_value = 0;
    while (true) {
        std::getline(items, line);
        if (items.eof()) break;
        if (line == "c") {
            items >> new_id >> new_level >> effect >> amount;
            std::getline(items, line);
            std::getline(items, new_name);
            std::getline(items, new_file_name);
            std::shared_ptr<Item> tmp = std::make_shared<Consumable>(new_id, new_level, new_name, new_file_name, effect, amount);
            item_list.insert(std::make_pair(new_id, tmp));
        }
        if (line == "w") {
            items >> new_id >> new_level >> stat_value >> effect >> amount;
            std::getline(items, line);
            std::getline(items, new_name);
            std::getline(items, new_file_name);
            std::shared_ptr<Item> tmp = std::make_shared<Weapon>(new_id, new_level, new_name, new_file_name,stat_value, effect, amount);
            item_list.insert(std::make_pair(new_id, tmp));
        }
        if (line == "a")
        {
            items >> new_id >> new_level >> stat_value >> effect;
            std::getline(items, line);
            std::getline(items, new_name);
            std::getline(items, new_file_name);
            std::shared_ptr<Item> tmp = std::make_shared<Armor>(new_id, new_level, new_name, new_file_name, stat_value, effect);
            item_list.insert(std::make_pair(new_id,tmp));
        }
    }
    items.close() ;
}

int Player::movePlayer(Map & map, char direction)
{
    switch(direction)
    {
        case 'w': {
            position.m_x--;
            switch (map.checkPosition(position, this)) {
                case 0 : {
                    map.updateLastPosition(position.m_x + 1, position.m_y);
                    break;
                }
                case 1 : {
                    position.m_x++;
                    break;
                }
                case 2 : {
                    inventory.push_back(item_list.find(map.findItem(position))->second);
                    position.m_x++;
                    break;
                }
                case 3 : {
                    map.updateLastPosition(position.m_x + 1, position.m_y);
                    position.m_x++;
                    break;
                }
                case 4 : {
                    map.updateLastPosition(position.m_x + 1, position.m_y);
                    return 1;
                }
            }
            break;
        }
        case 'd':{
            position.m_y++;
            switch (map.checkPosition(position, this)) {
                case 0 : {
                    map.updateLastPosition(position.m_x , position.m_y - 1);
                    break;
                }
                case 1 : {
                    position.m_y--;
                    break;
                }
                case 2 : {
                    inventory.push_back(item_list.find(map.findItem(position))->second);
                    position.m_y--;
                    break;
                }
                case 3 : {
                    map.updateLastPosition(position.m_x , position.m_y - 1);
                    position.m_y--;
                    break;
                }
                case 4 : {
                    map.updateLastPosition(position.m_x , position.m_y - 1);
                    return 1;
                }
            }
            break;
        }
        case 's':{
            position.m_x++;
            switch (map.checkPosition(position, this)) {
                case 0 : {
                    map.updateLastPosition(position.m_x - 1, position.m_y);
                    break;
                }
                case 1 : {
                    position.m_x--;
                    break;
                }
                case 2 : {
                    inventory.push_back(item_list.find(map.findItem(position))->second);
                    position.m_x--;
                    break;
                }
                case 3 : {
                    map.updateLastPosition(position.m_x - 1, position.m_y);
                    position.m_x--;
                    break;
                }
                case 4 : {
                    map.updateLastPosition(position.m_x - 1, position.m_y);
                    return 1;
                }
            }
            break;
        }
        case 'a':{
            position.m_y--;
            switch (map.checkPosition(position, this)) {
                case 0 : {
                    map.updateLastPosition(position.m_x , position.m_y + 1);
                    break;
                }
                case 1 : {
                    position.m_y++;
                    break;
                }
                case 2 : {
                    inventory.push_back(item_list.find(map.findItem(position))->second);
                    position.m_y++;
                    break;
                }
                case 3 : {
                    map.updateLastPosition(position.m_x , position.m_y + 1);
                    position.m_y++;
                    break;
                }
                case 4 : {
                    map.updateLastPosition(position.m_x , position.m_y + 1);
                    return 1;
                }
            }
            break;
        }
    }
    return 0;
}


bool Player::change_MP(int x)
{   if((player_stat.curMP == player_stat.maxMP) && x>0) return false;
    if(player_stat.curMP + x < 0) return false;
    player_stat.curMP += x;
    if(player_stat.curMP>player_stat.maxMP)
        player_stat.curMP = player_stat.maxMP;
    return true;}

void Player::change_STR(int x)
{stats_b.STR += x;
    stats_b.maxHP += x*25;
stats_b.curHP += x*25;}

void Player::change_AGI(int x)
{player_stat.AGI += x;}

void Player::change_INT(int x)
{player_stat.INT += x;
player_stat.maxMP += x*20;
    player_stat.curMP += x*20;}

void Player::change_armor(int x)
{
    player_stat.armor += x;
}

void Player::removeItem(size_t id_to_remove)
{
    std::vector<std::shared_ptr<Item>>::iterator iter;
    for(iter = inventory.begin(); iter!=inventory.end(); ++iter)
    {
        if(iter->get()->getId() == id_to_remove){
            inventory.erase(iter);
            break;
        }
    }
}

void Player::change_skillpoint(int x)
{
    player_stat.skill_points += x;
}

bool Player::change_gold(int x)
{
    if(player_stat.gold + x < 0) return false;
    player_stat.gold += x;
    return true;
}

void Player::change_exp(int x)
{
    player_stat.experience += x;
    if(player_stat.exp_needed < player_stat.experience)
    {
        m_level++;
        change_skillpoint(3);
        player_stat.experience -= player_stat.exp_needed;
        player_stat.exp_needed = 80*m_level;
    }
}

void Player::addToInventory(size_t id)
{
    inventory.push_back(item_list.find(id)->second);
}

bool Player::changeMap(std::string new_map)
{
 current_map = new_map;
 if(maps_visited.find(new_map) != maps_visited.end())
     return false;
    std::shared_ptr<Map> tmp_map;
 try
 {
      tmp_map = std::make_shared <Map>(new_map);
 }
 catch(std::string a) {
     std::cout << a << std::endl;
     return false;
 }
    maps_visited.insert(std::make_pair(new_map,tmp_map));
    return true;
}

void Player::save() const
{
    /**
     *  Clean save directory first.
     */
    std::string map_name;
    std::ifstream maps_delete ("save/map_list.txt");
    if(maps_delete.is_open())
    while (true) {
        std::getline(maps_delete, map_name);
        if (maps_delete.eof()) break;
        std::ostringstream os;
        os << "save/maps/" << map_name;
        std::remove (os.str().c_str());
        }
    std::remove("save/map_list.txt");
    std::remove("save/player_info.txt");
    std::remove("save/inventory.txt");
    std::remove("save/equiped.txt");
    /**
     * Then save the game to newly created .txt files.
     */
    std::ofstream player_file ("save/player_info.txt");
    player_file << m_name << "\n"
            << getMapName() << "\n"
            << position.m_x << " " << position.m_y << "\n"
            << m_level << " " << getSTR() << " " << getAGI() << " " << getINT() << " " << getSkillpoint() << "\n"
            << getHP() << "\n" << getMP() << "\n" << getExp() << "\n"
            << getGold() << " " << getArmor() << "\n";
    player_file.close();
    std::ofstream inventory_file ("save/inventory.txt");
    for(auto x : inventory)
        inventory_file << x.get()->getId() << "\n";
    inventory_file.close();
    std::ofstream equiped_file ("save/equiped.txt");
    for(int i=0;i<4;i++){
        if(equiped[i] == nullptr) {
        equiped_file << "-1" << "\n";
        }
        else {
        equiped_file << equiped[i]->getId() << "\n";
        }
    }
    equiped_file.close();
    std::ofstream map_list_file ("save/map_list.txt");
    for(auto x : maps_visited)
    {
        map_list_file << x.second.get()->getMapName() << "\n";
        std::ofstream tmp_file ("save/maps/" + x.second.get()->getMapName());
        for (unsigned int i = 0; i < x.second.get()->getHeight(); ++i) {
            for (unsigned int j = 0; j < x.second.get()->getWidth(); ++j) {
                tmp_file << x.second.get()->getMapPoint(i,j) << " ";
            }
            tmp_file << "\n";
        }
    }
    map_list_file.close();
}

bool Player::load()
{
    std::string tmp;
    std::ifstream load ("save/player_info.txt");
    if(!load.is_open()) return false;
    for(int i=0; i<4;i++)
    std::getline(load, tmp);
    int curHP, curMP, curExp,gold, armor, tmp_int;
    load >> curHP >> tmp >> tmp_int;
    if( tmp != "/") return false;
    load >> curMP >> tmp >> tmp_int;
    if( tmp != "/") return false;
    load >> curExp >> tmp >> tmp_int;
    if( tmp != "/") return false;
    load >> gold >> armor;
    stats_b.curHP = curHP;
    player_stat.curMP = curMP;
    player_stat.experience = curExp;
    player_stat.gold = gold;
    player_stat.armor = armor;
    load.close();
    int item_id;
    std::ifstream inventory_file ("save/inventory.txt");
    if(!inventory_file.is_open()){return false;}
    while(1)
    {
        inventory_file >> item_id;
        if(inventory_file.eof()) break;
        inventory.push_back(item_list.find(item_id)->second);
    }
    inventory_file.close();
    std::ifstream equiped_file ("save/equiped.txt");
    if(!equiped_file.is_open()){return false;}
    for(unsigned int i=0;i<4;i++)
    {
        equiped_file >> item_id;
        if(item_id != -1)
        {
            equiped[i] = item_list.find(item_id)->second.get();
        }
    }
    equiped_file.close();
    std::ifstream map_list_file ("save/map_list.txt");
    if(!map_list_file.is_open()){return false;}
    for(std::string map_name; std::getline(map_list_file,map_name);)
    {

        auto tmp = std::make_shared<Map>(map_name);
        if(!tmp.get()->reloadMap("save/maps/" + map_name)) return false;
        maps_visited.insert(std::make_pair(map_name,tmp));
    }
    map_list_file.close();
    return true;
}
