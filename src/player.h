
#ifndef SEMESTRALKA_PLAYER_H
#define SEMESTRALKA_PLAYER_H
#include <fstream>
#include <stdio.h>
#include "map.h"
#include "combat_entity.h"
#include "consumable.h"
#include "weapon.h"
#include "armor.h"

/**
 *  Class with all informations about player.
 */

class Player : public CombatEntity
{
public:
    Player(std::string name, int level, int x, int y, std::string mapname, BaseStats stats_b, PlayerStats stats_p);
    /**
     * After changing position based on direction calls map.checkPosition to react to the change
     * @return Returns 1 if there's Monster in new position, else returns 1
     */
    int movePlayer(Map & map, char direction);
    void addToInventory(size_t id);
    MapPosition getPos() const {return this->position;}
    std::vector<std::shared_ptr<Item>> getInventory() const {return inventory;}
    /**
     * \defgroup Get Get methods, returning strings
     * Get methods that return string, methods mainly used for printing out on screen
     * @return String for TTF_RenderText
     * @{
     */

    std::string getExp() const { return player_stat.getExp();}
    std::string getMP() const { return player_stat.getMP();}
    std::string getGold() const
    { std::ostringstream os; os << player_stat.gold;return os.str();}
    std::string getSTR() const
    { std::ostringstream os; os << stats_b.STR; return os.str();}
    std::string getAGI() const
    { std::ostringstream os; os << player_stat.AGI;return os.str();}
    std::string getINT() const
    { std::ostringstream os; os << player_stat.INT;return os.str();}
    std::string getArmor() const
    { std::ostringstream os; os << player_stat.armor; return os.str();}
    std::string getSkillpoint() const
    { std::ostringstream os; os << player_stat.skill_points; return os.str();}
    bool skillpoint_avaible() const {if(player_stat.skill_points != 0) return true; else return false;}
    std::string getMapName() const { return current_map;}

    /** @} */

    /**
     * \defgroup Change Those methods change values of parameters of objects
     * @return Bool if we need to check if value isn't below zero, else void
     * @{
     */
    bool change_MP(int x) ;
    void change_STR(int x);
    void change_AGI(int x);
    void change_INT(int x);
    void change_skillpoint(int x);
    void change_armor(int x);
    bool change_gold(int x);
    void change_exp(int x);
    /** @} */
    /**
     * Removes item from inventory based on its id
     */
    void removeItem(size_t id_to_remove);
    Item* & getEquiped(int position) {return equiped[position];}
    Item* getEquipCopy(int position) const {return equiped[position];}
    /**
     *  Changes current map of player.
     *  @return False if new map is already loaded in maps_visited.
     *  If not, returns true and creates new Map object and adds it to maps_visited.
     */
    bool changeMap(std::string new_map);
    Map * getMap(std::string x) const {return maps_visited.find(x)->second.get();}
/**
 * Deletes previous saved game and replaces it with current one
 */
    void save() const;
    /**
     * Loads data from saved game, if there's one.
     * @return False if any files aren't present or are in different format than expected
     */
    bool load();

private:
    /**
     *  Name of map that player is currently on
     */
    std::string current_map;
    /**
     *  Player-specific stats
     */
      PlayerStats player_stat;
    /**
     *  Vector of items in inventory
     */
    std::vector<std::shared_ptr<Item>> inventory;
    /**
     *  Array of items currently used by player
     */
    Item* equiped[4] = {nullptr};
    /**
    *  All game items, that are available in game
     */
    std::map<size_t,std::shared_ptr<Item>> item_list;
    /**
     *  All maps that were visited and influenced by player
     */
    std::map<std::string,std::shared_ptr<Map>> maps_visited;

};


#endif //SEMESTRALKA_PLAYER_H
