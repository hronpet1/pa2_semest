#include "stats.h"

BaseStats::BaseStats( int strength) :
        curHP(strength*25), maxHP(strength*25), STR(strength)
{}

std::string BaseStats::getHP() const
{
    std::ostringstream os;
    os << curHP << " / " << maxHP;
    return os.str();
}

PlayerStats::PlayerStats( int agility,  int intelligence,  int skill_point):
       curMP(intelligence*20), maxMP(intelligence*20), AGI(agility), INT(intelligence),  gold(0), experience(0), exp_needed(0), armor(0), skill_points(skill_point)
{}

std::string PlayerStats::getExp() const
{
    std::ostringstream os;
    os << experience << " / " << exp_needed;
    return os.str();
}


std::string PlayerStats::getMP() const
{
    std::ostringstream os;
    os << curMP << " / " << maxMP;
    return os.str();
}