#ifndef SEMESTRALKA_STATS_H
#define SEMESTRALKA_STATS_H

#include <string>
#include <iostream>
#include <sstream>

/**
 * Stats class that's same for all classes derived from CombatEntity
 */

class BaseStats
{
public:
    /// Constructor - only strength is needed, Health points are calculated based on that attribute
    BaseStats(int strength );
    std::string getHP() const;

    int curHP;
    int maxHP;
    int STR;
};

/**
 *  Player-specific stats
 */
class PlayerStats
{
public:
    PlayerStats(int agility, int intelligence, int skill_point);
    std::string getExp() const;
    std::string getMP() const;

    int curMP;
    int maxMP;
    int AGI;
    int INT;
    int gold;
    int experience;
    int exp_needed;
    int armor;
    int skill_points;
};


#endif //SEMESTRALKA_STATS_H
