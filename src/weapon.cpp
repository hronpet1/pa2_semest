#include "weapon.h"
#include "player.h"
Weapon::Weapon(size_t id, int level, std::string name, std::string file, int damage, int type, int amount):
Item(id, level, name, file), m_damage(damage), m_type(static_cast<Weapon::required_stat>(type)), type_amount(amount)
{}

bool Weapon::UseItem(Player &itemUser)
{
    /// Checks for requirements
    switch(m_type)
    {
        case NONE: break;
        case STR:
        {
            if(std::stoi(itemUser.getSTR()) < type_amount) return false;
            break;
        }
        case AGI:
        {
            if(std::stoi(itemUser.getAGI()) < type_amount) return false;
            break;
        }
        case INT:
        {
            if(std::stoi(itemUser.getINT()) < type_amount) return false;
            break;
        }
    }
    if(m_level > std::stoi(itemUser.getLevel())) return false;
    /// Switches currently equiped Item ( if there's one ) and the one to be equiped
    Item* tmp = itemUser.getEquiped(0);
    itemUser.removeItem(this->m_id);
    if(tmp != nullptr) tmp->UnequipItem(itemUser);
    itemUser.getEquiped(0) = this;
    return true;
}

void Weapon::UnequipItem(Player &itemUser)
{
    itemUser.addToInventory(m_id);
    itemUser.getEquiped(0) = nullptr;
}

void Weapon::PrintItem(SDL_Surface *surface, int x, int y)
{
    SDL_Rect position;
    position.x = x - 5 ; position.y = y - 5;
    SDL_Surface * tmp = IMG_Load("surfaces/item_info.bmp");
    SDL_BlitSurface(tmp, NULL, surface, &position);
    SDL_FreeSurface(tmp);
    position.x = x; position.y = y;
    std::ostringstream os;
    os << m_name ;
    printLine(surface, position, os);
    os << "Required level : " << m_level;
    position.y += 15;
    printLine(surface, position, os);
    os <<  "Bonus damage : " << m_damage;
    position.y += 15;
    printLine(surface, position, os);
    if(m_type != NONE)
    {
        os << "Requires " << type_amount << " " << getTextForEnum(static_cast<int>(m_type));
        position.y += 15;
        printLine(surface, position, os);
    }
}