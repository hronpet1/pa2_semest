#ifndef SEMESTRALKA_WEAPON_H
#define SEMESTRALKA_WEAPON_H

#include "item.h"

class Player;

/**
 * Items with offensive values
 */

class Weapon : public Item
{
public:
    /**
     *  Possible stats required to use item
     */
    enum required_stat{
        NONE,
        STR,
        AGI,
        INT
    };
    /// Strings for item info purposes
    std::string EnumStrings[4] = { "Nothing", "Strength", "Agility", "Intelligence" };
    std::string getTextForEnum( int enumVal )
    {
        return EnumStrings[enumVal];
    }
    Weapon(size_t id, int level, std::string name, std::string file, int damage, int type, int amount);
    bool UseItem(Player & itemUser);
    void UnequipItem(Player & itemUser);
    void PrintItem(SDL_Surface* surface, int x, int y);
    int getDmg() const {return m_damage;}
private:
    int m_damage;
    required_stat m_type;
    int type_amount;
};

#endif //SEMESTRALKA_WEAPON_H